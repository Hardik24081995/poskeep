package com.poskeep.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import com.poskeep.R
import com.poskeep.adapter.ReviewAdapter
import kotlinx.android.synthetic.main.fragment_about.*


class AboutFragment : Fragment() {
    lateinit var chipArray: ArrayList<String>
    lateinit var mParent: View
    var adapter: ReviewAdapter? = null

    companion object {
        fun getInstance(bundle: Bundle): AboutFragment {
            val fragment = AboutFragment()
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        mParent = inflater.inflate(R.layout.fragment_about, container, false)
        return mParent
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        chipArray = ArrayList();
        setChipList()

    }

    private fun setChipList() {
        chipArray.add("Hair")
        chipArray.add("Massage")
        chipArray.add("Nail")
        chipArray.add("Spa")
        chipArray.add("Barber")
        chipArray.add("Training")
        chipArray.add("Makeup")
        chipArray.add("Hair Removel")
        chipArray.add("All")

        setupRecyclerViewMarchant()
    }

    fun setupRecyclerViewMarchant() {
        rvReview.isNestedScrollingEnabled = false
        val layoutManager = GridLayoutManager(requireContext(), 1)
        rvReview.layoutManager = layoutManager
        adapter = ReviewAdapter(requireContext(), chipArray)
        rvReview.adapter = adapter
    }

}