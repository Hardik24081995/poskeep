package com.poskeep.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.poskeep.R
import com.poskeep.activity.MapActivity
import com.poskeep.activity.MarchantDtailActivity
import com.poskeep.activity.SearchMarchantActivity
import com.poskeep.adapter.MarchantAdapter
import com.poskeep.extention.goToActivity
import com.poskeep.extention.visible
import kotlinx.android.synthetic.main.reclerview_swipelayout.*
import kotlinx.android.synthetic.main.toolbar_with_back_arrow.*


class HomeFragment : BaseFragment(), MarchantAdapter.OnItemSelected {

    var marchantAdapter: MarchantAdapter? = null
    lateinit var chipArray: ArrayList<String>
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_home, container, false)
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        chipArray = ArrayList()
        setChipList()
        txtTitle.text ="Marchent List"
        imgAdd.visible()
        imgAdd.setImageResource(R.drawable.ic_search)
        imgAdd.setColorFilter(ContextCompat.getColor(requireContext(), R.color.white), android.graphics.PorterDuff.Mode.SRC_IN);

        imgAdd.setOnClickListener {
            goToActivity<SearchMarchantActivity>()
        }
    }

    private fun setChipList() {
        chipArray.add("Hair")
        chipArray.add("Massage")
        chipArray.add("Nail")
        chipArray.add("Spa")
        chipArray.add("Barber")
        chipArray.add("Training")
        chipArray.add("Makeup")
        chipArray.add("Hair Removel")
        chipArray.add("All")

        setupRecyclerViewMarchant()
    }
    fun setupRecyclerViewMarchant() {

        val layoutManager = LinearLayoutManager(requireContext())
        recyclerView.layoutManager = layoutManager
        marchantAdapter = MarchantAdapter(requireContext(), chipArray, this)
        recyclerView.adapter = marchantAdapter

    }

    override fun onItemSelect(position: Int, data: String) {
        goToActivity<MarchantDtailActivity>()
    }
}