package com.poskeep.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.poskeep.R
import com.poskeep.activity.CMSActivity
import com.poskeep.activity.ChangePasswordActivity
import com.poskeep.extention.goToActivity
import kotlinx.android.synthetic.main.fragment_setting.*
import kotlinx.android.synthetic.main.toolbar_with_back_arrow.*


class SettingsFragment : Fragment() {



    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val root = inflater.inflate(R.layout.fragment_setting, container, false)
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        txtTitle.text = "SETTING"

        relayPwd.setOnClickListener { goToActivity<ChangePasswordActivity>() }
        relayAboutus.setOnClickListener { goToActivity<CMSActivity>() }
        relayPrivacy.setOnClickListener { goToActivity<CMSActivity>() }
        relayTerms.setOnClickListener { goToActivity<CMSActivity>() }
    }
}