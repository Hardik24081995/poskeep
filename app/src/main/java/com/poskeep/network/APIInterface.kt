package com.sprinters.network

import com.poskeep.modal.*
import io.reactivex.Observable
import okhttp3.RequestBody
import retrofit2.Response
import retrofit2.http.*

interface APIInterface {

    @POST("home/login")
    fun login(@Body body: RequestBody): Observable<Response<UserModal>>

    @POST("common/category/getCategories")
    fun getCategory(): Observable<Response<CategoryModal>>

    @POST("common/country/getCountries")
    fun getCountry(): Observable<Response<CountryModal>>

    @POST("common/state/getStates")
    fun getState(@Body body: RequestBody): Observable<Response<StateModal>>


    @POST("home/signup")
    fun signup(@Body body: RequestBody): Observable<Response<UserModal>>
}
