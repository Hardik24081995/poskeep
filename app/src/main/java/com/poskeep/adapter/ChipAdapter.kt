package com.poskeep.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.poskeep.R
import com.robertlevonyan.views.chip.Chip
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.row_chipview.*

class ChipAdapter(
    private val mContext: Context,
    var list: MutableList<String> = mutableListOf()
) : RecyclerView.Adapter<ChipAdapter.ItemHolder>() {

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemHolder {
        return ItemHolder(
            LayoutInflater.from(mContext).inflate(
                R.layout.row_chipview,
                parent, false
            )
        )
    }

    override fun onBindViewHolder(holder: ItemHolder, position: Int) {
        val data = list[position]

        holder.bindData(mContext, data)
    }

    /* interface OnItemSelected {
         fun onItemSelect(position: Int, data: Datum)
     }*/

    class ItemHolder(override val containerView: View) :
        RecyclerView.ViewHolder(containerView),
        LayoutContainer {


        fun bindData(
            context: Context,
            data: String
        ) {
            var txtName = containerView.findViewById<TextView>(R.id.txtName)
            txtName.text= data

            //chips.text= data

        }

    }
}