package com.poskeep.modal

import com.google.gson.annotations.SerializedName

data class CategoryModal(

	@field:SerializedName("data")
	val data: List<CategoryData> = mutableListOf(),

	@field:SerializedName("error")
	val error: Boolean? = null,

	@field:SerializedName("message")
	val message: String? = null
)

 data class CategoryData(

	@field:SerializedName("Rno")
	val rno: String? = null,

	@field:SerializedName("rowcount")
	val rowcount: String? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("status")
	val status: String? = null
)
