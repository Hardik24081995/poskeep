package com.poskeep.modal

import com.google.gson.annotations.SerializedName

data class UserModal(

	@field:SerializedName("data")
	val data: Data? = null,

	@field:SerializedName("error")
	val error: Boolean? = null,

	@field:SerializedName("message")
	val message: String? = null
)

data class Data(

	@field:SerializedName("stores")
	val stores: List<StoreData> = mutableListOf(),

	@field:SerializedName("user")
	val user: User? = null
)

data class User(

	@field:SerializedName("country")
	val country: String? = null,

	@field:SerializedName("firstname")
	val firstname: String? = null,

	@field:SerializedName("rowcount")
	val rowcount: String? = null,

	@field:SerializedName("city")
	val city: String? = null,

	@field:SerializedName("mobile")
	val mobile: String? = null,

	@field:SerializedName("profile_pic")
	val profilePic: String? = null,

	@field:SerializedName("created_at")
	val createdAt: String? = null,

	@field:SerializedName("email_verified_at")
	val emailVerifiedAt: Any? = null,

	@field:SerializedName("deleted_at")
	val deletedAt: Any? = null,

	@field:SerializedName("lastname")
	val lastname: String? = null,

	@field:SerializedName("zipcode")
	val zipcode: String? = null,

	@field:SerializedName("is_admin")
	val isAdmin: String? = null,

	@field:SerializedName("password")
	val password: String? = null,

	@field:SerializedName("Rno")
	val rno: String? = null,

	@field:SerializedName("updated_at")
	val updatedAt: String? = null,

	@field:SerializedName("address_line_1")
	val addressLine1: String? = null,

	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("address_line_2")
	val addressLine2: String? = null,

	@field:SerializedName("state")
	val state: String? = null,

	@field:SerializedName("remember_token")
	val rememberToken: Any? = null,

	@field:SerializedName("email")
	val email: String? = null,

	@field:SerializedName("status")
	val status: String? = null
)

data class StoreData(

	@field:SerializedName("thursday_end_min")
	val thursdayEndMin: String? = null,

	@field:SerializedName("instructions")
	val instructions: String? = null,

	@field:SerializedName("country")
	val country: String? = null,

	@field:SerializedName("wednesday_status")
	val wednesdayStatus: String? = null,

	@field:SerializedName("thursday_end_hour")
	val thursdayEndHour: String? = null,

	@field:SerializedName("saturday_start_min")
	val saturdayStartMin: String? = null,

	@field:SerializedName("logo")
	val logo: Any? = null,

	@field:SerializedName("saturday_end_min")
	val saturdayEndMin: String? = null,

	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("saturday_start_hour")
	val saturdayStartHour: String? = null,

	@field:SerializedName("state_id")
	val stateId: String? = null,

	@field:SerializedName("state")
	val state: String? = null,

	@field:SerializedName("monday_start_hour")
	val mondayStartHour: String? = null,

	@field:SerializedName("wednesday_start_min")
	val wednesdayStartMin: String? = null,

	@field:SerializedName("friday_start_min")
	val fridayStartMin: String? = null,

	@field:SerializedName("zipcode")
	val zipcode: String? = null,

	@field:SerializedName("user_id")
	val userId: String? = null,

	@field:SerializedName("friday_status")
	val fridayStatus: String? = null,

	@field:SerializedName("phone")
	val phone: String? = null,

	@field:SerializedName("short_name")
	val shortName: String? = null,

	@field:SerializedName("saturday_end_hour")
	val saturdayEndHour: String? = null,

	@field:SerializedName("country_id")
	val countryId: String? = null,

	@field:SerializedName("status")
	val status: String? = null,

	@field:SerializedName("thursday_start_min")
	val thursdayStartMin: String? = null,

	@field:SerializedName("friday_end_hour")
	val fridayEndHour: String? = null,

	@field:SerializedName("tuesday_end_hour")
	val tuesdayEndHour: String? = null,

	@field:SerializedName("tuesday_start_min")
	val tuesdayStartMin: String? = null,

	@field:SerializedName("city")
	val city: String? = null,

	@field:SerializedName("cancelation_policy")
	val cancelationPolicy: String? = null,

	@field:SerializedName("monday_end_hour")
	val mondayEndHour: String? = null,

	@field:SerializedName("description")
	val description: String? = null,

	@field:SerializedName("created_at")
	val createdAt: String? = null,

	@field:SerializedName("wednesday_end_min")
	val wednesdayEndMin: String? = null,

	@field:SerializedName("sunday_start_min")
	val sundayStartMin: String? = null,

	@field:SerializedName("friday_end_min")
	val fridayEndMin: String? = null,

	@field:SerializedName("thursday_start_hour")
	val thursdayStartHour: String? = null,

	@field:SerializedName("category_id")
	val categoryId: String? = null,

	@field:SerializedName("kid_friendly")
	val kidFriendly: String? = null,

	@field:SerializedName("address_line_1")
	val addressLine1: String? = null,

	@field:SerializedName("store_name")
	val storeName: String? = null,

	@field:SerializedName("tuesday_status")
	val tuesdayStatus: String? = null,

	@field:SerializedName("sunday_status")
	val sundayStatus: String? = null,

	@field:SerializedName("address_line_2")
	val addressLine2: String? = null,

	@field:SerializedName("sunday_end_min")
	val sundayEndMin: String? = null,

	@field:SerializedName("monday_status")
	val mondayStatus: String? = null,

	@field:SerializedName("sunday_end_hour")
	val sundayEndHour: String? = null,

	@field:SerializedName("monday_end_min")
	val mondayEndMin: String? = null,

	@field:SerializedName("website")
	val website: String? = null,

	@field:SerializedName("sunday_start_hour")
	val sundayStartHour: String? = null,

	@field:SerializedName("banner")
	val banner: Any? = null,

	@field:SerializedName("disabled_access")
	val disabledAccess: String? = null,

	@field:SerializedName("wifi_access")
	val wifiAccess: String? = null,

	@field:SerializedName("tuesday_end_min")
	val tuesdayEndMin: String? = null,

	@field:SerializedName("spoken_languages")
	val spokenLanguages: String? = null,

	@field:SerializedName("parking_fees")
	val parkingFees: String? = null,

	@field:SerializedName("saturday_status")
	val saturdayStatus: String? = null,

	@field:SerializedName("wednesday_start_hour")
	val wednesdayStartHour: String? = null,

	@field:SerializedName("friday_start_hour")
	val fridayStartHour: String? = null,

	@field:SerializedName("monday_start_min")
	val mondayStartMin: String? = null,

	@field:SerializedName("tuesday_start_hour")
	val tuesdayStartHour: String? = null,

	@field:SerializedName("thursday_status")
	val thursdayStatus: String? = null,

	@field:SerializedName("plan_id")
	val planId: String? = null,

	@field:SerializedName("wednesday_end_hour")
	val wednesdayEndHour: String? = null
)
