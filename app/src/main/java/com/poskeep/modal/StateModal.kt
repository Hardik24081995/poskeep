package com.poskeep.modal

import com.google.gson.annotations.SerializedName

data class StateModal(

	@field:SerializedName("data")
	val data: List<StateData> = mutableListOf(),

	@field:SerializedName("error")
	val error: Boolean? = null,

	@field:SerializedName("message")
	val message: String? = null
)

data class StateData(

	@field:SerializedName("country_code")
	val countryCode: String? = null,

	@field:SerializedName("wikiDataId")
	val wikiDataId: String? = null,

	@field:SerializedName("Rno")
	val rno: String? = null,

	@field:SerializedName("flag")
	val flag: String? = null,

	@field:SerializedName("updated_at")
	val updatedAt: String? = null,

	@field:SerializedName("rowcount")
	val rowcount: String? = null,

	@field:SerializedName("fips_code")
	val fipsCode: String? = null,

	@field:SerializedName("created_at")
	val createdAt: String? = null,

	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("state")
	val state: String? = null,

	@field:SerializedName("iso2")
	val iso2: String? = null,

	@field:SerializedName("country_id")
	val countryId: String? = null
)
