package com.poskeep.modal

import com.google.gson.annotations.SerializedName

data class CountryModal(

	@field:SerializedName("data")
	val data: List<CountryData> = mutableListOf(),

	@field:SerializedName("error")
	val error: Boolean? = null,

	@field:SerializedName("message")
	val message: String? = null
)

  data  class CountryData(

	@field:SerializedName("country")
	val country: String? = null,

	@field:SerializedName("capital")
	val capital: String? = null,

	@field:SerializedName("emojiU")
	val emojiU: String? = null,

	@field:SerializedName("wikiDataId")
	val wikiDataId: String? = null,

	@field:SerializedName("flag")
	val flag: String? = null,

	@field:SerializedName("emoji")
	val emoji: String? = null,

	@field:SerializedName("subregion")
	val subregion: String? = null,

	@field:SerializedName("created_at")
	val createdAt: String? = null,

	@field:SerializedName("phonecode")
	val phonecode: String? = null,

	@field:SerializedName("updated_at")
	val updatedAt: String? = null,

	@field:SerializedName("native")
	val jsonMemberNative: String? = null,

	@field:SerializedName("currency")
	val currency: String? = null,

	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("iso2")
	val iso2: String? = null,

	@field:SerializedName("region")
	val region: String? = null,

	@field:SerializedName("iso3")
	val iso3: String? = null
)
