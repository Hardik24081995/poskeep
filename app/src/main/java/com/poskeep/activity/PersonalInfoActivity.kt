package com.poskeep.activity

import android.content.Intent
import android.os.Bundle
import com.blogspot.atifsoftwares.animatoolib.Animatoo
import com.poskeep.R
import com.poskeep.extention.*
import com.sprinters.utils.Constant
import kotlinx.android.synthetic.main.activity_personalinfo.*
import kotlinx.android.synthetic.main.activity_personalinfo.edtEmail


class PersonalInfoActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_personalinfo)

        btnNext.setOnClickListener { checkValidation() }

        txtLogin.setOnClickListener { goToActivityAndClearTask<LoginActivity>() }
    }

    fun checkValidation() {
        when {
            edtFirstname.isEmpty() -> {
                root.showSnackBar("Enter Firstname")
                edtFirstname.requestFocus()
            }
            !isValidName(edtFirstname.getValue()) -> {
                root.showSnackBar("Enter Valid Firstname")
                edtFirstname.requestFocus()
            }
            edtLastname.isEmpty() -> {
                root.showSnackBar("Enter Lastname")
                edtLastname.requestFocus()
            }
            !isValidName(edtLastname.getValue()) -> {
                root.showSnackBar("Enter Valid Lastname")
                edtLastname.requestFocus()
            }
            edtEmail.isEmpty() -> {
                root.showSnackBar("Enter Email")
                edtEmail.requestFocus()
            }
            !isValidEmail(edtEmail.getValue()) -> {
                root.showSnackBar("Enter Valid Email")
                edtEmail.requestFocus()
            }
            edtMobile.isEmpty() -> {
                root.showSnackBar("Enter Mobile No.")
                edtMobile.requestFocus()
            }

            else -> {
                sendData()
            }

        }
    }

    fun sendData() {
        val i = Intent(this, BusinessInfoActivity::class.java)
        i.putExtra(Constant.FNAME, edtFirstname.getValue())
        i.putExtra(Constant.LNAME, edtLastname.getValue())
        i.putExtra(Constant.EMAIL, edtEmail.getValue())
        i.putExtra(Constant.MOBILE, edtMobile.getValue())
        startActivity(i)
        Animatoo.animateCard(this)
    }
}