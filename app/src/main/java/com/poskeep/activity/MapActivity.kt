package com.poskeep.activity

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.poskeep.R
import com.poskeep.adapter.MarchantAdapter
import com.poskeep.extention.goToActivity
import kotlinx.android.synthetic.main.activity_map.*
import kotlinx.android.synthetic.main.bottomsheet_map.*


class MapActivity : BaseActivity(), OnMapReadyCallback, MarchantAdapter.OnItemSelected {

    private lateinit var mMap: GoogleMap
    lateinit var chipArray: ArrayList<String>
    var marchantAdapter: MarchantAdapter? = null
    private lateinit var bottomSheetBehavior: BottomSheetBehavior<ConstraintLayout>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_map)
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        imgBack.setOnClickListener {
            finish()
        }
        bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet)


        bottomSheetBehavior.addBottomSheetCallback(object :
            BottomSheetBehavior.BottomSheetCallback() {

            override fun onSlide(bottomSheet: View, slideOffset: Float) {
                // handle onSlide
            }

            override fun onStateChanged(bottomSheet: View, newState: Int) {
                when (newState) {

                    BottomSheetBehavior.STATE_COLLAPSED -> {
                        //    Toast.makeText(this@MapActivity, "STATE_COLLAPSED", Toast.LENGTH_SHORT).show()
                    }

                    BottomSheetBehavior.STATE_EXPANDED -> {
                        //    Toast.makeText(this@MapActivity, "STATE_EXPANDED", Toast.LENGTH_SHORT).show()
                    }

                    BottomSheetBehavior.STATE_DRAGGING -> {
                        //    Toast.makeText(this@MapActivity, "STATE_DRAGGING", Toast.LENGTH_SHORT).show()
                    }

                    BottomSheetBehavior.STATE_SETTLING -> {
                        //    Toast.makeText(this@MapActivity, "STATE_SETTLING", Toast.LENGTH_SHORT).show()
                    }

                    BottomSheetBehavior.STATE_HIDDEN -> {
                        //    Toast.makeText(this@MapActivity, "STATE_HIDDEN", Toast.LENGTH_SHORT).show()
                    }
                    else -> {
                        //Toast.makeText(this@MapActivity, "OTHER_STATE", Toast.LENGTH_SHORT).show()
                    }
                }
            }
        })
//            btnBottomSheetPersistent.setOnClickListener {
//                if (bottomSheetBehavior.state == BottomSheetBehavior.STATE_EXPANDED)
//                    bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
//                else
//                    bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
//            }


        chipArray = ArrayList()
        setChipList()
    }

    private fun setChipList() {
        chipArray.add("Hair")
        chipArray.add("Massage")
        chipArray.add("Nail")
        chipArray.add("Spa")
        chipArray.add("Barber")
        chipArray.add("Training")
        chipArray.add("Makeup")
        chipArray.add("Hair Removel")
        chipArray.add("All")


        setupRecyclerViewMarchant()
    }

    fun setupRecyclerViewMarchant() {

        val layoutManager = LinearLayoutManager(this)
        rvMarchant.layoutManager = layoutManager
        marchantAdapter = MarchantAdapter(this, chipArray, this)
        rvMarchant.adapter = marchantAdapter
    }


    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        // Add a marker in Sydney and move the camera
        val sydney = LatLng(-34.0, 151.0)
        mMap.addMarker(
            MarkerOptions()
                .position(sydney)
                .title("Marker in Sydney")
        )
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney))
    }

    override fun onItemSelect(position: Int, data: String) {
        goToActivity<MarchantDtailActivity>()
    }
}