package com.poskeep.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.poskeep.R
import com.poskeep.adapter.ChipAdapter
import com.poskeep.adapter.MarchantAdapter
import com.poskeep.extention.goToActivity
import com.poskeep.extention.goToActivityAndClearTask
import com.poskeep.extention.visible
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.toolbar_with_back_arrow.*


class SearchMarchantActivity : AppCompatActivity(), MarchantAdapter.OnItemSelected {
    lateinit var chipArray: ArrayList<String>
    var adapter: ChipAdapter? = null
    var marchantAdapter: MarchantAdapter? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        chipArray = ArrayList()
        setChipList()
        imgBack.visible()
        imgBack.setImageResource(R.drawable.ic_back)
        txtTitle.text="Marchant Search"
        imgBack.setOnClickListener { finish() }
    }

    private fun setChipList() {
        chipArray.add("Hair")
        chipArray.add("Massage")
        chipArray.add("Nail")
        chipArray.add("Spa")
        chipArray.add("Barber")
        chipArray.add("Training")
        chipArray.add("Makeup")
        chipArray.add("Hair Removel")
        chipArray.add("All")

        setupRecyclerViewCategory()
        setupRecyclerViewMarchant()
    }

    fun setupRecyclerViewCategory() {

        val layoutManager = GridLayoutManager(this, 3)
        rvChip.layoutManager = layoutManager
        adapter = ChipAdapter(this, chipArray)
        rvChip.adapter = adapter
    }

    fun setupRecyclerViewMarchant() {

        val layoutManager = LinearLayoutManager(this)
        rvMarchant.layoutManager = layoutManager
        marchantAdapter = MarchantAdapter(this, chipArray, this)
        rvMarchant.adapter = marchantAdapter

        btnSearch.setOnClickListener { goToActivity<MapActivity>() }
    }

    override fun onItemSelect(position: Int, data: String) {
        goToActivity<MarchantDtailActivity>()
    }


}