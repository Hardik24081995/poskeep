package com.poskeep.activity

import android.os.Bundle

import com.poskeep.R
import com.poskeep.extention.*
import com.poskeep.modal.UserModal
import com.sprinters.network.CallbackObserver
import com.sprinters.network.Networking
import com.sprinters.network.addTo
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_changepassword.*
import kotlinx.android.synthetic.main.toolbar_with_back_arrow.*


class ChangePasswordActivity :BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_changepassword)

        btnSubmit.setOnClickListener { goToActivityAndClearTask<LoginActivity>() }
        imgBack.visible()
        imgBack.setImageResource(R.drawable.ic_back)
        txtTitle.text = "Change Password"
        imgBack.setOnClickListener { finish() }
    }

   /* fun validation(){

        when {

            edtEmail.isEmpty() -> {
                root.showSnackBar("Enter Email")
                edtEmail.requestFocus()
            }
            !isValidEmail(edtEmail.getValue()) -> {
                root.showSnackBar("Enter Valid Email")
                edtEmail.requestFocus()
            }
            edtPassword.isEmpty() -> {
                root.showSnackBar("Enter Password")
                edtPassword.requestFocus()
            }
            *//*edtPassword.getValue().length <6 ->{
                root.showSnackBar("Enter Minimum 6 Character")
                edtPassword.requestFocus()
            }*//*
            else -> {
                login()
            }

        }

    }

     fun login() {
        showProgressbar()
        val params = HashMap<String, Any>()
        params["email"] = edtEmail.getValue()
        params["password"] = edtPassword.getValue()

        Networking
            .with(this)
            .getServices()
            .login(Networking.wrapParams(params))//wrapParams Wraps parameters in to Request body Json format
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeWith(object : CallbackObserver<UserModal>() {
                override fun onSuccess(response: UserModal) {
                    val data = response.data
                    hideProgressbar()
                    if(response.error==false){
                        if (data != null ) {
                            session.user =response
                            goToActivityAndClearTask<SearchMarchantActivity>()
                        } else {
                            showAlert(response.message.toString())
                        }
                    }else{
                        showAlert(response.message.toString())
                    }

                }

                override fun onFailed(code: Int, message: String) {
                    showAlert(message)
                    hideProgressbar()
                }

            }).addTo(autoDisposable)
    }*/

}