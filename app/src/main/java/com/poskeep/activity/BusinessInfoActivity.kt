package com.poskeep.activity

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.blogspot.atifsoftwares.animatoolib.Animatoo
import com.poskeep.R
import com.poskeep.extention.*
import com.poskeep.modal.*
import com.sprinters.network.CallbackObserver
import com.sprinters.network.Networking
import com.sprinters.network.addTo
import com.sprinters.utils.Constant
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_businessinfo.*
import kotlinx.android.synthetic.main.activity_businessinfo.btnBack
import kotlinx.android.synthetic.main.activity_businessinfo.btnNext
import kotlinx.android.synthetic.main.activity_businessinfo.root
import kotlinx.android.synthetic.main.activity_businessinfo.txtLogin
import kotlinx.android.synthetic.main.activity_personalinfo.*


class BusinessInfoActivity : BaseActivity() {
    val categoryList: MutableList<CategoryData> = mutableListOf()
    val countryList: MutableList<CountryData> = mutableListOf()
    val stateList: MutableList<StateData> = mutableListOf()
    val catNameList: MutableList<String> = mutableListOf()
    val countyNameList: MutableList<String> = mutableListOf()
    val stateNameList: MutableList<String> = mutableListOf()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_businessinfo)

        btnNext.setOnClickListener { checkValidation() }

        btnBack.setOnClickListener { finish() }

        txtLogin.setOnClickListener { goToActivityAndClearTask<LoginActivity>() }
        getCaegory()
        spinnerListner()
    }

    fun checkValidation() {
        when {
            spCategory.selectedItemPosition == 0 -> {
                root.showSnackBar("Select Category")
                spCategory.requestFocus()
            }
            edtBusnessname.isEmpty() -> {
                root.showSnackBar("Enter Busness Name")
                edtBusnessname.requestFocus()
            }
            edtSorName.isEmpty() -> {
                root.showSnackBar("Enter Sort Name")
                edtSorName.requestFocus()
            }
            spCountry.selectedItemPosition == 0 -> {
                root.showSnackBar("Select Country")
                spCountry.requestFocus()
            }
            spState.selectedItemPosition == 0 -> {
                root.showSnackBar("Select State")
                spState.requestFocus()
            }
            edtCity.isEmpty() -> {
                root.showSnackBar("Enter City Name")
                edtCity.requestFocus()
            }
            edtAddress1.isEmpty() -> {
                root.showSnackBar("Enter Address Line 1")
                edtAddress1.requestFocus()
            }
            edtAddress2.isEmpty() -> {
                root.showSnackBar("Enter Address Line 2")
                edtAddress2.requestFocus()
            }
            edtZipCode.isEmpty() -> {
                root.showSnackBar("Enter Zipcode")
                edtZipCode.requestFocus()
            }
            else -> {
                sendData()
            }
        }
    }

    fun sendData() {
        val catID= categoryList.get(spCategory.selectedItemPosition-1).id.toString()
        val countyID= countryList.get(spCountry.selectedItemPosition-1).id.toString()
        val stateID= stateList.get(spState.selectedItemPosition-1).id.toString()
        val i = Intent(this, SecurityInfoActivity::class.java)
        i.putExtra(Constant.FNAME, intent.getStringExtra(Constant.FNAME))
        i.putExtra(Constant.LNAME, intent.getStringExtra(Constant.LNAME))
        i.putExtra(Constant.EMAIL, intent.getStringExtra(Constant.EMAIL))
        i.putExtra(Constant.MOBILE, intent.getStringExtra(Constant.MOBILE))
        i.putExtra(Constant.CAT_ID, catID)
        i.putExtra(Constant.COUNTRY_ID, countyID)
        i.putExtra(Constant.STATE_ID, stateID)
        i.putExtra(Constant.BUSNESSNAME, edtBusnessname.getValue())
        i.putExtra(Constant.ADDRESS1, edtAddress1.getValue())
        i.putExtra(Constant.ADDRESS2, edtAddress2.getValue())
        i.putExtra(Constant.ZIPCODE, edtZipCode.getValue())
        startActivity(i)
        Animatoo.animateCard(this)
    }

    fun setCateSpData() {
        // Creating adapter for spinner
        catNameList.add("Select Category")
        for (i in categoryList.indices) {
            catNameList.add(categoryList.get(i).name.toString())
        }

        val dataAdapter: ArrayAdapter<String> =
            ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, catNameList)
        spCategory.adapter = dataAdapter

        getCountry()
    }


    fun setCountrySpData() {
        // Creating adapter for spinner
        countyNameList.add("Select County")
        for (i in countryList.indices) {
            countyNameList.add(countryList.get(i).country.toString())
        }

        val dataAdapter: ArrayAdapter<String> =
            ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, countyNameList)
        spCountry.adapter = dataAdapter
    }


    fun setStateSpData() {
        // Creating adapter for spinner
        stateNameList.add("Select State")
        for (i in stateList.indices) {
            stateNameList.add(stateList.get(i).state.toString())
        }

        val dataAdapter: ArrayAdapter<String> =
            ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, stateNameList)
        spState.adapter = dataAdapter
    }


    fun spinnerListner() {
        spCountry.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (spCountry.selectedItemPosition != 0) {
                    val stateid = countryList.get(spCountry.selectedItemPosition - 1).id.toString()
                    getState(stateid)
                } else {
                    stateList.clear()
                    stateNameList.clear()
                    setStateSpData()
                }
            }

        }
    }

    fun getCaegory() {
        showProgressbar()
        Networking
            .with(this)
            .getServices()
            .getCategory()//wrapParams Wraps parameters in to Request body Json format
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeWith(object : CallbackObserver<CategoryModal>() {
                override fun onSuccess(response: CategoryModal) {
                    hideProgressbar()
                    if (response.error == false) {
                        categoryList.addAll(response.data)
                        setCateSpData()
                    } else {
                        showAlert(response.message.toString())
                    }

                }

                override fun onFailed(code: Int, message: String) {
                    showAlert(message)
                    hideProgressbar()
                }

            }).addTo(autoDisposable)
    }

    fun getCountry() {
        showProgressbar()
        Networking
            .with(this)
            .getServices()
            .getCountry()//wrapParams Wraps parameters in to Request body Json format
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeWith(object : CallbackObserver<CountryModal>() {
                override fun onSuccess(response: CountryModal) {
                    hideProgressbar()
                    if (response.error == false) {
                        countryList.addAll(response.data)
                        setCountrySpData()

                    } else {
                        showAlert(response.message.toString())
                    }

                }

                override fun onFailed(code: Int, message: String) {
                    showAlert(message)
                    hideProgressbar()
                }

            }).addTo(autoDisposable)
    }

    fun getState(id: String) {
        showProgressbar()
        stateList.clear()
        stateNameList.clear()
        val params = HashMap<String, Any>()
        params["country_id"] = id

        Networking
            .with(this)
            .getServices()
            .getState(Networking.wrapParams(params))//wrapParams Wraps parameters in to Request body Json format
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeWith(object : CallbackObserver<StateModal>() {
                override fun onSuccess(response: StateModal) {
                    hideProgressbar()
                    if (response.error == false) {
                        stateList.addAll(response.data)
                        setStateSpData()

                    } else {
                        showAlert(response.message.toString())
                    }

                }

                override fun onFailed(code: Int, message: String) {
                    showAlert(message)
                    hideProgressbar()
                }

            }).addTo(autoDisposable)
    }
}