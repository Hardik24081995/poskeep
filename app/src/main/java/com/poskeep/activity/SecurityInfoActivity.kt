package com.poskeep.activity

import android.os.Bundle
import com.poskeep.R
import com.poskeep.extention.*
import com.poskeep.modal.UserModal
import com.sprinters.network.CallbackObserver
import com.sprinters.network.Networking
import com.sprinters.network.addTo
import com.sprinters.utils.Constant
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_securityinfo.*
import kotlinx.android.synthetic.main.activity_securityinfo.btnBack
import kotlinx.android.synthetic.main.activity_securityinfo.edtPassword
import kotlinx.android.synthetic.main.activity_securityinfo.root
import kotlinx.android.synthetic.main.activity_securityinfo.txtLogin


class SecurityInfoActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_securityinfo)

        btnSubmit.setOnClickListener { checkValidation() }
        btnBack.setOnClickListener { finish() }
        txtLogin.setOnClickListener { goToActivityAndClearTask<LoginActivity>() }
    }

    fun checkValidation() {
        when {
            edtPassword.isEmpty() -> {
                root.showSnackBar("Enter Password")
                edtPassword.requestFocus()
            }
            edtPassword.getValue().length < 6 -> {
                root.showSnackBar("Password length must be atleast 6 characters")
                edtPassword.requestFocus()
            }
            edtConfirmPwd.isEmpty() -> {
                root.showSnackBar("Enter Confirm Password")
                edtConfirmPwd.requestFocus()
            }
            !edtPassword.getValue().equals(edtConfirmPwd.getValue()) ->{
                root.showSnackBar("Password Does Not Matched")
                edtPassword.requestFocus()
            }
            else -> {
              signUp()
            }
        }
    }

    fun signUp() {
        showProgressbar()

        val params = HashMap<String, Any>()
        params["firstname"] = intent.getStringExtra(Constant.FNAME).toString()
        params["lastname"] = intent.getStringExtra(Constant.LNAME).toString()
        params["email"] = intent.getStringExtra(Constant.EMAIL).toString()
        params["mobile"] = intent.getStringExtra(Constant.MOBILE).toString()
        params["category_id"] = intent.getStringExtra(Constant.CAT_ID).toString()
        params["store_name"] = intent.getStringExtra(Constant.BUSNESSNAME).toString()
        params["short_name"] = intent.getStringExtra(Constant.SORTNAME).toString()
        params["country"] = intent.getStringExtra(Constant.COUNTRY_ID).toString()
        params["state"] = intent.getStringExtra(Constant.STATE_ID).toString()
        params["city"] = intent.getStringExtra(Constant.CITY).toString()
        params["address_line_1"] = intent.getStringExtra(Constant.ADDRESS1).toString()
        params["address_line_2"] = intent.getStringExtra(Constant.ADDRESS2).toString()
        params["zipcode"] = intent.getStringExtra(Constant.ZIPCODE).toString()
        params["plan_id"] = "1"
        params["password"] = edtPassword.getValue()


        Networking
            .with(this)
            .getServices()
            .signup(Networking.wrapParams(params))//wrapParams Wraps parameters in to Request body Json format
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeWith(object : CallbackObserver<UserModal>() {
                override fun onSuccess(response: UserModal) {
                    val data = response.data
                    hideProgressbar()
                    if(response.error==false){
                        if (data != null ) {
                            session.user =response
                            goToActivityAndClearTask<SearchMarchantActivity>()
                        } else {
                            showAlert(response.message.toString())
                        }
                    }else{
                        showAlert(response.message.toString())
                    }

                }

                override fun onFailed(code: Int, message: String) {
                    showAlert(message)
                    hideProgressbar()
                }

            }).addTo(autoDisposable)
    }


}