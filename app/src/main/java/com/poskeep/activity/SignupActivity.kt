package com.poskeep.activity

import android.content.Intent
import android.os.Bundle
import com.blogspot.atifsoftwares.animatoolib.Animatoo
import com.poskeep.R
import com.poskeep.extention.*
import com.sprinters.utils.Constant


import kotlinx.android.synthetic.main.activity_signup.*


class SignupActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signup)

        btnRegister.setOnClickListener { goToActivityAndClearTask<HomeActivity>() }

        txtLogin.setOnClickListener { goToActivityAndClearTask<LoginActivity>() }
    }


}