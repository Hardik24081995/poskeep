package com.poskeep.activity

import android.os.Bundle
import android.os.Handler
import android.view.Window
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide

import com.poskeep.R
import com.poskeep.extention.goToActivityAndClearTask

import kotlinx.android.synthetic.main.activity_splash.*

class SplashActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        );
        setContentView(R.layout.activity_splash)
        applyAnimation()
        Handler().postDelayed({
            validateRedirection()
        }, 2500)
        /*  Glide.with(this).load(R.drawable.splashgif)
              .into(imageView)*/

    }

    private fun validateRedirection() {
        if (session.isLoggedIn )
           // goToActivityAndClearTask<MainActivity>()
        goToActivityAndClearTask<LoginActivity>()
        else
            goToActivityAndClearTask<LoginActivity>()
    }

    fun applyAnimation() {
        /*YoYo.with(Techniques.Wobble)
            .duration(2500)
            .repeat(0)
            .playOn(imageView);*/
    }
}