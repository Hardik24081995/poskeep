package com.poskeep.activity

import android.os.Bundle
import android.util.DisplayMetrics
import android.util.Log
import android.view.View
import android.view.ViewTreeObserver
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.tabs.TabLayout
import com.poskeep.R
import com.poskeep.adapter.ViewPagerPagerAdapter
import com.poskeep.extention.visible
import com.poskeep.fragment.AboutFragment
import kotlinx.android.synthetic.main.activity_marchant_detail.*
import kotlinx.android.synthetic.main.bottomsheet_details.*


class MarchantDtailActivity : AppCompatActivity() {

    var viewPageradapter: ViewPagerPagerAdapter? = null
    private lateinit var bottomSheetBehavior: BottomSheetBehavior<ConstraintLayout>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_marchant_detail)
        imgBack.visible()
        imgBack.setImageResource(R.drawable.ic_back)
        txtTitle.text = "Marchant Detail"
        setStatePageAdapter()
        appbar()

        imgBack.setOnClickListener { finish() }

        bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet)


        val displayMetrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(displayMetrics)

        var height = displayMetrics.heightPixels


        bottomSheetBehavior.peekHeight = height - 256


        bottomSheetBehavior.addBottomSheetCallback(object :
            BottomSheetBehavior.BottomSheetCallback() {

            override fun onSlide(bottomSheet: View, slideOffset: Float) {
                // handle onSlide
            }

            override fun onStateChanged(bottomSheet: View, newState: Int) {
                when (newState) {

                    BottomSheetBehavior.STATE_COLLAPSED -> {
                        //    Toast.makeText(this@MapActivity, "STATE_COLLAPSED", Toast.LENGTH_SHORT).show()
                    }

                    BottomSheetBehavior.STATE_EXPANDED -> {
                        //    Toast.makeText(this@MapActivity, "STATE_EXPANDED", Toast.LENGTH_SHORT).show()
                    }

                    BottomSheetBehavior.STATE_DRAGGING -> {
                        //    Toast.makeText(this@MapActivity, "STATE_DRAGGING", Toast.LENGTH_SHORT).show()
                    }

                    BottomSheetBehavior.STATE_SETTLING -> {
                        //    Toast.makeText(this@MapActivity, "STATE_SETTLING", Toast.LENGTH_SHORT).show()
                    }

                    BottomSheetBehavior.STATE_HIDDEN -> {
                        //    Toast.makeText(this@MapActivity, "STATE_HIDDEN", Toast.LENGTH_SHORT).show()
                    }
                    else -> {
                        //Toast.makeText(this@MapActivity, "OTHER_STATE", Toast.LENGTH_SHORT).show()
                    }
                }
            }
        })

    }

    private fun appbar(){
//        app_bar_profile.addOnOffsetChangedListener(object : OnOffsetChangedListener {
//            var isShow = false
//            var scrollRange = -1
//            override fun onOffsetChanged(appBarLayout: AppBarLayout, verticalOffset: Int) {
//                if (scrollRange == -1) {
//                    scrollRange = appBarLayout.totalScrollRange
//                }
//                if (scrollRange + verticalOffset == 0) {
//                    isShow = true
//                   // mTextHeader.setVisibility(View.VISIBLE)
//                } else if (isShow) {
//                    isShow = false
//                   // mTextHeader.setVisibility(View.VISIBLE)
//                }
//            }
//        })
    }

    private fun seTabs() {
        tabs.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                view_pager.currentItem = tab.position
                val fm = supportFragmentManager
                val ft = fm.beginTransaction()
                val count = fm.backStackEntryCount
                if (count >= 1) {
                    supportFragmentManager.popBackStack()
                }
                ft.commit()
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {
                // setAdapter();


            }

            override fun onTabReselected(tab: TabLayout.Tab) {

                //   viewPager.notifyAll();
            }
        })
    }

    private fun setStatePageAdapter() {


        viewPageradapter = ViewPagerPagerAdapter(supportFragmentManager)
        viewPageradapter?.addFragment(AboutFragment(), "About")
        viewPageradapter?.addFragment(AboutFragment(), "Staff")
        viewPageradapter?.addFragment(AboutFragment(), "Services")
        viewPageradapter?.addFragment(AboutFragment(), "Gift Cards")
        view_pager.adapter = viewPageradapter
        tabs.setupWithViewPager(view_pager, true)

    }



}