package com.sprinters.interfaces

/**
 * Created by HARDIK
 */

interface SnackbarActionListener {
    fun onAction()
}
