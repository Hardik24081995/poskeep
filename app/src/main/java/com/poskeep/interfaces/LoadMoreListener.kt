package com.sprinters.interfaces

/**
 * Created by HARDIK
 */

interface LoadMoreListener {
    fun onLoadMore()
}
